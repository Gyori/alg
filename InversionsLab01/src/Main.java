import inversions.Inversions;

/**
 * Gyori Robert 531 lab01
 */
public class Main {

    public static void main(String[] args) {

        //array from https://www.youtube.com/watch?v=MxiQa22KrSQ
        int[] a = {1, 5, 4, 8, 10, 2, 6, 9, 12, 11, 3, 7};
        System.out.println(Inversions.count(a));
    }
}
