package inversions;

public class Inversions {

    public static int count(int[] a) {

        return count(a, 0, a.length);
    }

    private static int count(int[] a, int left, int right) {

        //n == 1 => 0 inversions
        if (left + 1 == right) {

            return 0;
        }

        int middle = (left + right) / 2;
        int leftCount = count(a, left, middle);
        int rightCount = count(a, middle, right);
        int mergeCount = merge(a, left, middle, right);

        return leftCount + rightCount + mergeCount;
    }

    private static int merge(int[] a, int left, int middle, int right) {

        int inversions = 0;
        int left2 = left;
        int left3 = left;
        int middle2 = middle;
        int aux[] = new int[a.length];

        while (left2 < right) {

            if (right <= middle2 || left3 < middle && a[middle2] >= a[left3]) {

                aux[left2] = a[left3];
                left3++;
            } else {

                aux[left2] = a[middle2];
                inversions += middle - left3;
                middle2++;
            }

            left2++;
        }

        System.arraycopy(aux, left, a, left, right - left);
        return inversions;
    }
}
