package com.company;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        Majority majority = new Majority();

        Integer winner = majority.get();

        if (null == winner) {

            System.out.println("There is no majority..");
        } else {

            System.out.println("Winner is: " + winner);
        }
    }
}
