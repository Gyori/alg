package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Majority {

    private final static String file = "numbers";

    public Majority() {
    }

    public Integer get() throws FileNotFoundException {

        List<Integer> values = this.getValues();

        int left = 0;
        int right = values.size() - 1;

        return searchForMajority(left, right, values);

    }

    private Integer searchForMajority(int left, int right, List<Integer> values) {

        Integer middle;
        Integer majorityFromLeft;
        Integer majorityFromRight;

        if (left > right) {

            return null;
        }

        if (left == right) {

            return values.get(left);
        }

        middle = left + (right - left) / 2;
        majorityFromLeft = searchForMajority(left, middle, values);
        majorityFromRight = searchForMajority(middle + 1, right, values);

        return returnMajority(left, right, values, majorityFromLeft, majorityFromRight);

    }

    private Integer returnMajority(int left, int right, List<Integer> values, Integer majorityFromLeft, Integer majorityFromRight) {

        int minim = (right - left + 1) / 2;
        if (null == majorityFromLeft && null != majorityFromRight) {

            if (minim < countOccurrences(left, right, majorityFromRight, values)) {

                return majorityFromRight;
            }

            return null;
        }

        if (null != majorityFromLeft && null == majorityFromRight) {

            if (minim < countOccurrences(left, right, majorityFromLeft, values)) {

                return majorityFromLeft;
            }

            return null;
        }

        if (null == majorityFromLeft && null == majorityFromRight) {

            return null;
        }

        if (null != majorityFromLeft && null != majorityFromRight) {

            if (minim < countOccurrences(left, right, majorityFromLeft, values)) {

                return majorityFromLeft;
            }

            if (minim < countOccurrences(left, right, majorityFromRight, values)) {

                return majorityFromRight;
            }
        }
        if (majorityFromLeft.equals(majorityFromRight)) {

            return majorityFromLeft;
        } else {

            return null;
        }

    }

    private int countOccurrences(int left, int right, Integer value, List<Integer> values) {

        int counter = 0;

        for (int i = left; i <= right; i++) {

            if (value.equals(values.get(i))) {
                counter++;
            }
        }

        return counter;
    }

    private List<Integer> getValues() throws FileNotFoundException {

        List<Integer> values;
        Scanner scanner;

        scanner = new Scanner(new FileReader(file));
        values = new ArrayList<>();

        while (scanner.hasNextInt()) {

            values.add(scanner.nextInt());
        }

        return values;
    }

}
